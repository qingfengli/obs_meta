# Preinstall：编译环境预装rpm，与install不同，他们是直接本地解压的，不需要安装依赖
Preinstall: glibc bash rpm rpm-libs
Preinstall: libcap libacl libdb bzip2 zlib xz-libs zstd lua popt openssl-libs attr
Preinstall: libselinux pcre2 dbus-libs systemd-libs lz4 libgcrypt libgpg-error
Preinstall: filesystem
Preinstall: digest-list-tools selinux-policy-targeted pesign-obs-integration
#https://gitee.com/openeuler/digest-list-tools/issues/I4VUK0?from=project-issue

# Support：这类rpm在构建环境启动之后再安装，区别在于这些rpm不会影响构建结果
Support: chkconfig
Support: util-linux
Support: rpm-build
Support: make
#Support: custom_build_tool-nocheck
Support: gcc_secure

# order：在启动构建环境中存在安装依赖环或者指定预装的包缺少依赖时，order将决定从哪里破环
Order: setup:shadow
Order: filesystem:glibc
Order: filesystem:bash
Order: filesystem:vim-filesystem
Order: filesystem:emacs-filesystem
Order: filesystem:acl
Order: filesystem:attr
Order: filesystem:libgcc
Order: filesystem:setup
Order: ncurses-libs:filesystem
Order: coreutils:openssl-libs
Order: audit:systemd
Order: cryptsetup:unbound-libs
Order: cryptsetup:openssh-server

# BuildFlags：定义构建过程的关键字标志
BuildFlags: logidlelimit:1800 # 单个job打印编译日志的超时机制

# FileProvides：为了减少rpm的files列表占用大量内存，使用FileProvides来直接告知构建系统哪些rpm提供了某个文件，注意，文件必须是全路径
FileProvides: /usr/bin/fipscheck  fipscheck
FileProvides: /usr/bin/db_stat    libdb
FileProvides: /usr/bin/gdb-add-index gdb-headless
FileProvides: /usr/bin/gpg2 gnupg2 
FileProvides: /usr/bin/ruby       ruby
FileProvides: /usr/bin/python python3-unversioned-command
FileProvides: /usr/bin/python3 python3
FileProvides: /usr/bin/dtrace systemtap-sdt-devel
FileProvides: /usr/sbin/ifconfig net-tools

Conflict: nss-softokn nss-softokn-freebl

# Prefer: 当有多个rpm提供同名provides时，为了确保构建可复制，默认优先选择一个，否则系统会上报unresolvable
# 对于EBS来说，可以默认都不要，便于识别真实的冲突，最终推动社区改进
Prefer: libseccomp wqy-zenhei-fonts openssl-devel gobject-introspection python3-wrapt
Prefer: openEuler-rpm-config
Prefer: openEuler-release
Prefer: openEuler-logos
Prefer: openEuler-repos
Prefer: openEuler-indexhtml
#custom prefer for openEuler
Prefer: autogen texinfo glx-utils
Prefer: jbig2dec java-1.8.0-openjdk java-1.8.0-openjdk-devel java-1.8.0-openjdk-javadoc 
Prefer: fontpackages-filesystem
Prefer: lxpolkit rubygem-minitest python3-capstone python-mock
Prefer: sblim-sfcb crda clamav-data pkcs11-helper-devel Lmod lua
Prefer: selinux-policy-mls selinux-policy-minimum
Prefer: binutils-devel
Prefer: postfix
Prefer: kernel kernel-devel
Prefer: python-setuptools
Prefer: elinks
Prefer: libverto-libev
Prefer: rubygem-rspec
Prefer: emacs
Prefer: gnuplot
Prefer: gnome-themes-standard
Prefer: jbigkit-libs
Prefer: libcdio-paranoia libcdio
Prefer: glassfish-el-api glassfish-servlet-api 
Prefer: perl-libs fonts-filesystem apr libnghttp2 dnf dnf-plugins-core gstreamer1-plugins-base jboss-websocket-1.0-api
Prefer: nodejs-nan0 uglify-js boost-devel protobuf-java protobuf-devel
Prefer: ccid libglvnd-devel device-mapper-persistent-data dbus-libs rpm-libs
Prefer: ima-evm-utils
Prefer: maven pipewire
Prefer: jetty-http jetty-io jetty-security jetty-server jetty-servlet jetty-util
# 2022 02 16
Prefer: jakarta-el jakarta-server-pages-api jakarta-server-pages jakarta-mail jakarta-el-api
#2021 09 10#
Prefer: openssl-libs
#2022 03 22#
Prefer:  tinyxml2 ocaml-camlp4 ocaml-ocamlbuild ocaml-csexp

# 体现在替换spec里面的buildrequire，不会影响间接依赖，与FileProvides异曲同工
# 对于EBS来说，可以默认都不要，便于识别真实的冲突，最终推动社区改进
Substitute: /usr/bin/protoc protobuf # 可以用protobuf替换软件包spec中BuildRequires字段/usr/bin/protoc
Substitute: %{_bindir}/pathfix.py python3-devel
Substitute: %{_bindir}/nsgmls opensp
Substitute: %{_bindir}/ps procps-ng
Substitute: %{_bindir}/sgml2xml opensp
Substitute: python-dbus python3-dbus
Substitute: /usr/sbin/useradd shadow
Substitute: /usr/sbin/groupadd shadow
Substitute: /usr/sbin/userdel shadow
Substitute: /usr/sbin/groupdel shadow
Substitute: /usr/bin/pod2man perl-podlators
Substitute: %{_bindir}/dtrace systemtap-sdt-devel
Substitute: %{_bindir}/cmake cmake
Substitute: %{_bindir}/sphinx-build python3-sphinx
Substitute:  /usr/sbin/sendmail sendmail
Substitute:  /usr/bin/pod2man perl-podlators
Substitute:  /usr/bin/pdflatex texlive-latex
Substitute:  /usr/bin/makeindex texlive-makeindex
Substitute: /lib64/libc.so.6 glibc
Substitute: /usr/lib64/libc.so glibc-devel
Substitute: /usr/bin/xsltproc libxslt
Substitute: /etc/init.d chkconfig
Substitute: %{_includedir}/linux/if.h kernel-headers
Substitute: /usr/bin/yelp-build yelp-tools
Substitute: /usr/bin/ducktype python3-mallard-ducktype
Substitute: /usr/bin/chrpath chrpath
Substitute: /usr/bin/tclsh tcl
Substitute: /usr/bin/file file
Substitute: /usr/bin/base64 coreutils
Substitute: /usr/bin/head coreutils
Substitute: /usr/bin/sha256sum coreutils
Substitute: /usr/bin/tr coreutils
Substitute: /usr/bin/jq jq
Substitute: %{_bindir}/pkg-config pkgconf
Substitute: /usr/bin/gtk-update-icon-cache gtk-update-icon-cache
Substitute: /usr/bin/c++ gcc-c++
Substitute: %{_bindir}/sphinx-build-3 python3-sphinx
Substitute: /usr/bin/awk gawk
Substitute: /usr/bin/pathfix.py python3-devel
Substitute: /usr/bin/doxygen doxygen
Substitute: /usr/bin/g-ir-scanner gobject-introspection-devel
Substitute: %{_bindir}/rst2html python3-docutils
Substitute: %{_bindir}/valac vala
Substitute: /usr/bin/pygmentize python3-pygments
Substitute: /usr/bin/perl perl
Substitute: /usr/bin/iconv glibc-common
Substitute: %{_bindir}/hostname hostname
Substitute: %{_bindir}/a2x asciidoc
Substitute: /usr/bin/certtool gnutls-utils
Substitute: /usr/bin/socat socat
Substitute: /usr/sbin/ss iproute
Substitute: /usr/bin/cmp diffutils
Substitute: /usr/bin/rename util-linux
Substitute: /usr/sbin/sysctl procps-ng
Substitute: %{_bindir}/libgcrypt-config libgcrypt-devel
Substitute: /usr/bin/man man-db
Substitute: /usr/bin/dtrace systemtap-sdt-devel
Substitute: /usr/sbin/ifconfig net-tools
Substitute: /usr/bin/dbus-launch dbus-x11
Substitute: /usr/bin/2to3 python3-devel
Substitute: %{_bindir}/xsltproc libxslt
Substitute: %{_bindir}/sed sed
Substitute: python%{python3_pkgversion}-devel python3-devel
Substitute: python%{python3_pkgversion}-setuptools python3-setuptools
Substitute: python%{python3_pkgversion}-chardet python3-chardet
Substitute: python%{python3_pkgversion}-urllib3 python3-urllib3
Substitute: python%{python3_pkgversion}-idna python3-idna
Substitute: python%{python3_pkgversion}-pytest python3-pytest
Substitute: python%{python3_pkgversion}-pytest-cov python3-pytest-cov
Substitute: python%{python3_pkgversion}-pytest-httpbin python3-pytest-httpbin
Substitute: python%{python3_pkgversion}-pytest-mock python3-pytest-mock
Substitute: python%{python3_pkgversion}-pip python3-pip
Substitute: python%{python3_pkgversion}-wheel python3-wheel
Substitute: /usr/bin/dnf dnf
Substitute: /usr/bin/zip zip
Substitute: %{_prefix}/share/i18n/locales/de_DE glibc-locale-source
Substitute: /usr/bin/ssh-add openssh-clients
Substitute: /usr/bin/ssh-agent openssh-clients
Substitute: /usr/bin/ssh openssh-clients
Substitute: /usr/bin/appstream-util libappstream-glib
Substitute: /usr/bin/xmlto xmlto
Substitute: /usr/bin/pod2html perl
Substitute: /usr/sbin/mke2fs e2fsprogs
Substitute: %{_libdir}/krb5/plugins/kdb/db2.so krb5-server
Substitute: /usr/bin/gtk-encode-symbolic-svg gtk3-devel
Substitute: /usr/bin/dos2unix dos2unix
Substitute: /usr/bin/unix2dos dos2unix
Substitute: /usr/bin/which which
Substitute: /usr/bin/epstopdf texlive-epstopdf
Substitute: /usr/bin/desktop-file-validate desktop-file-utils
Substitute: /usr/bin/valac vala
Substitute: %{_bindir}/desktop-file-validate desktop-file-utils
Substitute: %{_bindir}/appstream-util libappstream-glib
Substitute: /usr/bin/texi2dvi texinfo-tex
Substitute: %{_bindir}/pod2html perl-podlators
Substitute: %{_bindir}/pod2man 	perl-podlators
Substitute: /usr/bin/pod2text perl-podlators
Substitute: /usr/bin/ping iputils
Substitute: /usr/bin/wget wget
Substitute: /usr/bin/qemu-img qemu-img
Substitute: /usr/bin/kreadconfig5 kf5-kconfig-core
Substitute: /usr/bin/xmllint libxml2
Substitute: /usr/bin/nc nmap
Substitute: /usr/bin/lsof lsof
Substitute: /usr/bin/ps procps-ng
Substitute: %{__perl} perl
Substitute: /usr/bin/rpcgen rpcgen
Substitute: %{_bindir}/latex texlive-latex
Substitute: %{_bindir}/memcached memcached
Substitute: /usr/bin/tox python3-tox
Substitute: %{_bindir}/2to3 python3-devel
Substitute: %{_bindir}/redis-server redis
Substitute: %{_bindir}/pkill procps-ng
Substitute: %{_bindir}/netstat net-tools
Substitute: %{_bindir}/gpg gnupg2
Substitute: %{__make} make
Substitute: %{_bindir}/podselect perl-Pod-Parser
Substitute: /usr/bin/autopoint gettext-devel
Substitute: %{_bindir}/perl perl
Substitute: %{_bindir}/openssl openssl
Substitute: %{_bindir}/man man-db
Substitute: /usr/bin/makeinfo texinfo
Substitute: %{_includedir}/magic.h file-devel
Substitute: %{_bindir}/python python-unversioned-command
Substitute: /usr/bin/getopt util-linux
Substitute: mysql-devel mariadb-connector-c-devel
Substitute: gnupg gnupg2
Substitute: /etc/pki/tls/certs/ca-bundle.crt ca-certificates

Release: %{?release} 

Macros:
%openeuler 1
%openEuler 1
%_specdir %{_topdir}/SOURCES

%_vendor openEuler
%_isa %{?__isa:(%{__isa})}%{!?__isa:%{nil}}
%vendor http://openeuler.org
%distribution  openEuler
%packager http://openeuler.org

#for sign
%_signature gpg
%_gpg_path /home/abuild/.gnupg
%_gpg_name private OBS
%__gpg_sign_cmd         %{__gpg} \
    gpg --no-verbose --no-armor \
    %{?_gpg_digest_algo:--digest-algo %{_gpg_digest_algo}} \
    --batch --pinentry-mode=loopback --passphrase="" \
    --no-secmem-warning \
    %{?_gpg_sign_cmd_extra_args:%{_gpg_sign_cmd_extra_args}} \
    -u "%{_gpg_name}" -sbo %{__signature_filename} %{__plaintext_filename}

%kernel_module_package_buildreqs kernel-devel openEuler-rpm-config

%with_ocaml 1
%with_python3 1
%with_python2 1
%python3_pkgversion 3
%golang_arches %{ix86} x86_64 %{arm} aarch64 ppc64le s390x
%efi_arch aa64
%efi_vendor openEuler
%efi aarch64 x86_64
%valgrind_arches %{ix86} x86_64 %{arm} aarch64 ppc64le s390x
%dist .oe2203sp1


# 2022-02-17 add automake
%gcc_secure_exclude "podman.spec|gcc.spec|arm-trusted-firmware.spec|docker-engine-openeuler.spec|arts.spec|runc-openeuler.spec|kata-containers.spec|dyninst.spec|ipxe.spec|tboot.spec|syslinux.spec|gcc-libraries.spec|kpatch.spec|gdb.spec|xorg-x11-server.spec|xorg-x11-drv-fbdev.spec|xorg-x11-drv-vesa.spec|xorg-x11-drv-ati.spec|xorg-x11-drv-dummy.spec|xorg-x11-drv-intel.spec|xorg-x11-drv-nouveau.spec|xorg-x11-drv-qxl.spec|xorg-x11-drv-v4l.spec|xorg-x11-drv-vmware.spec|xorg-x11-drv-evdev.spec|xorg-x11-drv-synaptics.spec|xorg-x11-drv-vmmouse.spec|xorg-x11-drv-void.spec|xorg-x11-drv-wacom.spec|hivex.spec|gimp.spec|memstomp.spec|supermin.spec|system-config-firewall.spec|isdn4k-utils.spec|emacs.spec|graphviz.spec|buildah.spec|rhash.spec|automake.spec"

%_build_id_links none

#custom macros
%openEuler 1
%disable_rpath \
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool \
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%delete_la_and_a \
find $RPM_BUILD_ROOT -type f -name "*.la" -delete \
find $RPM_BUILD_ROOT -type f -name "*.a" -delete

%delete_la  find $RPM_BUILD_ROOT -type f -name "*.la" -delete 

%chrpath_delete find $RPM_BUILD_ROOT/ -type f -exec file {} ';' | grep "\<ELF\>" | awk -F ':' '{print $1}' | xargs -i chrpath --delete {}

%package_help        \
%package        help \
Summary:        Documents for %{name} \
Buildarch:      noarch \
Requires:		man info \
\
%description help \
Man pages and other related documents for %{name}. 


%install_info() \
/sbin/install-info %1 %{_infodir}/dir || :

%install_info_rm() \
/sbin/install-info --remove %1 %{_infodir}/dir || :


%nocheck_exclude qt5-qtbase
%__brp_digest_list /usr/lib/rpm/brp-digest-list %{buildroot}
%nodejs_arches aarch64 x86_64
# Add for glibc by xiasenlin，2022/02/23, related issue:https://gitee.com/src-openeuler/obs_meta/issues/I4UXKX
%glibc_abort_after_test_fail 1
:Macros
